#include <iostream>
#include <fstream>
#include "LesData2.h"
#include "functions.h"
#include "tournament.h"
#include "player.h"
using namespace std;

vector <Tournament*> gTournaments;      ///< Tournaments
vector <Team*> gTeams;                  ///< Competing teams
vector <Player*> gPlayers;              ///< All players
Player* playerFunctions;

int main() {

    readFromFile();                    //Read all pre-registered data from file

    char kommando;

    writeMenu();                       //The different user options
    kommando = lesChar("\nCommand");

    //Runs the command according to user input
    while (kommando != 'Q') {
        switch (kommando) {
        case 'N': newTournament();        break;
        case 'T': newTeam();              break;
        case 'V': view();                 break;
        case 'U': updateTournament();     break;
        case 'D': deleteTournament();     break;
        }
        writeMenu();
        kommando = lesChar("\nCommand");
    }   
    writeToFile();                     //Write all registered data to file
    freeAllocatedMemory();             //Frees all allocated memory

    return 0;
}
