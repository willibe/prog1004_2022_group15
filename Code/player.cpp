#include "LesData2.h"
#include "player.h"
#include "team.h"
#include <fstream>
#include <string>
#include <algorithm>
#include <string>
#include <iostream>
using namespace std;

/**
 *  Constructor for reading player from file
 *
 * @param in - file to be read from
 */
Player::Player(ifstream& in) {
    getline(in, firstName);
    getline(in, lastName);
    in >> numGoals >> team;
    in.ignore();
}

/**
 *  Reads in the first name and last name of the player.
 */
void Player::readData() {
    cout << "\tWhats the last name of the new player?";
    getline(cin, lastName);
    cout << "\tWhats the first name of the new player?";
    getline(cin, firstName);

    cout << "Player '" << firstName << " " 
                                << lastName << "' was added to the team!\n\n";
}

/**
 *  Checks if the player firstName and lastName matches n and n2
 *
 *  @param n - first name
 *  @param n2 - last name
 *
 *  @return true if name matches, else false
 */
bool Player::hasName(string n, string n2) {
    //Makes all names in capital letters to make it non-case-sensitive:
    std::transform(n.begin(), n.end(), n.begin(), ::toupper);
    std::transform(n2.begin(), n2.end(), n2.begin(), ::toupper);

    string n3 = firstName;
    std::transform(n3.begin(), n3.end(), n3.begin(), ::toupper);
    string n4 = lastName;
    std::transform(n4.begin(), n4.end(), n4.begin(), ::toupper);
    //Checks if the player has this exact name:
    if (n3 == n && n4 == n2)
        return true;
    else
        return false;
}

/**
* Sets the private data members for the class
*
* @param fName - firstName, lname - lastName, goals - number of goals scored
*/
void Player::setData(string fName, string lName, int teamID) {
    firstName = fName;
    lastName = lName;
    team = teamID;
}

/**
 *  Writes out the first name and last name of the player.
 */
void Player::writeData() {
    // print out the name of player
    cout << "\t\nName of Player: " << firstName << " " << lastName;
    cout << "\t\n ";
}

/**
*   Writes player to file
*
*   @param out - the output file
*/
void Player::writeToFile(ofstream& out) {
    out << firstName << '\n';
    out << lastName << '\n';
    out << numGoals << " " << team << '\n';
}
