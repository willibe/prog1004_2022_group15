#ifndef __FUNCTIONS_H
#define __FUNCTIONS_H
#include "match.h"
#include <vector>
#include <string>

//General functions
void freeAllocatedMemory();
void readFromFile();
void view();
void viewPlayers();
void writeMenu();
void writeToFile();

//Team related
int  findTeam(std::string n);
void newTeam();
void viewTeams();

//Tournament related
bool allUpdated(std::vector <Match*> match);
bool checkDate(const int d, const int m, const int y);
void deleteAllTournaments();
void deleteASpecific(const int nr);
void deleteTournament();
void newTournament();
void updateTournament();
void viewTournaments();

#endif
