#ifndef __TOURNAMENT_H
#define __TOURNAMENT_H
#include <vector>
#include <string>
#include "match.h"
#include "team.h"
#include <fstream>
/**
 *  Base class 'Tournament'.
 *  Contains a name and date for the tournament, which teams
 *  that competed in the tournament, and the their matches.
 */
class Tournament {
private:
    int day, month, year;
    int numTeams;
    int tournamentStage;
    bool finished, playerGoals;
    std::string winnerName;
    std::string name;
    std::vector <Match*> matches;
    std::vector <Match*> sixteenTeams;
    std::vector <Match*> quarterFinal;
    std::vector <Match*> semiFinal;
    std::vector <Match*> final;
    std::vector <Team*> teams;

public:
    Tournament(const int d, const int m, const int y) {
        day = d;  month = m;  year = y; numTeams = 0; tournamentStage = 0;
        finished = playerGoals = false; winnerName = " ";
    };
    Tournament(std::ifstream& in);
    ~Tournament();
    void readInfo();
    void writeInfo(const int numMatches) const;
    void writeMatches(const int numMatches, std::vector <Match*> stage) const;
    void viewAll(const int numMatches) const;
    void nextStage();
    void logResult();
    int result(std::vector <Match*> matches);
    void teamsForNextStage(std::vector <Match*> matches, 
                   std::vector <Match*> matchesSize, int tmp, Match* newMatch);
    std::string returnName() { return name; }
    std::vector <Match*> returnMatches() { return matches; }
    void writeToFile(std::ofstream& out);
};

#endif // __TOURNAMENT_H