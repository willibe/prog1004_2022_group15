#include "LesData2.h"
#include "player.h"
#include "team.h"
#include <fstream>
#include <algorithm>
#include <string>
#include <iostream>
#include <iomanip>
using namespace std;
extern vector <Player*> gPlayers;
extern vector <Team*> gTeams;

/**
 *  Constructor for reading team from file
 *
 * @param in - file to read from
 * @param allData - if players and win/loss should be read
 */
Team::Team(ifstream& in, const bool allData) {
    //Stores all the private data from file
    if (allData == true) {
        int numPlayers, help;
        getline(in, teamName);
        in >> numPlayers;
        in.ignore();
        for (int i = 0; i < numPlayers; i++) {
            in >> help;
            players.push_back(help);
            in.ignore();
        }
        in >> numWins >> numLosses >> id;
        in.ignore();
    }
    else {
        getline(in, teamName);
    }
}

/**
* Constructor for teams sets the private data for the team
*
* @param team - the team to set the data for
*/
Team::Team(const Team& team) {
    teamName = team.teamName;
    //Sets the data for all the players
    for (int i = 0; i < team.players.size(); i++) {
        players.push_back(team.players[i]);
    }
    id = team.id;
}

/**
 *  Reads in the name of the team.
 *
 * @param addPlayers - if players should be added
 * @param addPlayers2 - if user should be asked about adding players
 * @see Player::hasName(...), Player::setData(...)
 */
void Team::readData(const bool addPlayers, const bool addPlayers2) {
    //  Player::ReadData();
    char wPlayers = 'Y';
    Player* newPlayer;
    string fName, lName;
    int playerID;
    id = gTeams.size();
    if (addPlayers2 == true) {
        do {
            wPlayers = lesChar("Do you want to add players?(Y/N)");
        } while (wPlayers != 'Y' && wPlayers != 'N');
    }
    if (wPlayers == 'Y' && addPlayers == true) {
        int numPlayers = lesInt("How many players are on the team?", 6, 99);
        for (int i = 0; i < numPlayers; i++) {  //For number of players
            playerID = 0;
            cout << "Whats the first name of the player?";
            getline(cin, fName);
            cout << "Whats the last name of the player?";
            getline(cin, lName);
            cout << endl;
            //Check if the name is already registered
            for (int i = 0; i < gPlayers.size(); i++) {
                if (gPlayers[i]->hasName(fName, lName))
                    playerID = -1;
            }
            if (playerID != -1) { //Name was not already registered
                newPlayer = new Player;
                newPlayer->setData(fName, lName, id);  //Set the players data
                players.push_back(gPlayers.size());    //Tournament players
                gPlayers.push_back(newPlayer);         //All players registered
            }
            else {                //Name was already registered
                cout << "\n\tThis player already exists!\n";
                i--;
            }
        }
    }
}


/**
 *  Writes out the name of the team
 *
 * @param all - if all data should be written
 * @see Player::returnFirstName(), Player::returnLastName()
 * @see Player::returnNumGoals()
 */
void Team::writeData(bool all) const {
    cout << teamName; // print out the name of team
    cout << "\n\tWin/Loss: " << numWins << '/' << numLosses;
    cout << endl;
    if (all && players.size() > 0) {    //Players are registered for this team
        cout << "\n\tPlayers:\n";
        string name;
        for (int i = 0; i < players.size(); i++) {  //For all players
            //Write the players data
            name = gPlayers[players[i]]->returnFirstName() + " "
                + gPlayers[players[i]]->returnLastName();
            cout << '\t'
                << left << setw(40) << name << "\tGoals: "
                << gPlayers[players[i]]->returnNumGoals() << endl;
        }
    }
}

/**
 *  Checks if the team name matches n
 *
 * @param n - name to be checked against
 * @return true if name matches, else false
 */
bool Team::hasName(string n) {
    string n2 = teamName;
    std::transform(n2.begin(), n2.end(), n2.begin(), ::toupper);

    for (int i = 0; i < n.size(); i++) {
        if (n[i] != n2[i])              //if name is not equal to @param
            return false;
    }
    return true;
}

/**
*   Writes Team to file
*
*   @param out - the output file
*/
void Team::writeToFile(ofstream& out, const bool allData) {
    //Writes all the private data of the team to file
    if (allData == true) {
        out << teamName << '\n';
        out << players.size() << '\n';
        for (int i = 0; i < players.size(); i++) {
            out << players[i] << '\n';
        }
        out << numWins << " " << numLosses << " " << id << '\n';
    }
    else {
        out << teamName << '\n';
    }
}
