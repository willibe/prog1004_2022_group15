#include "tournament.h"
#include "match.h"
#include "LesData2.h"
#include "team.h"
#include "functions.h "
#include <vector>
#include <string>
#include <iostream>
using namespace std;
extern vector <Team*> gTeams;
/**
 *  Constructor for reading tournament from file
 *
 * @param in - the file to be read
 * @see Match::returnT1(), Match::returnT2()
 */
Tournament::Tournament(ifstream& in) {

    bool stageComplete = false;
    bool stage1Complete = false;
    bool stage2Complete = false;
    bool stage3Complete = false;

    //Simple private data read from file
    in >> day >> month >> year; in.ignore();
    getline(in, name);
    in >> numTeams; in.ignore();
    in >> tournamentStage; in.ignore();
    in >> finished; in.ignore();
    in >> playerGoals; in.ignore();
    if (finished) { in >> winnerName; in.ignore(); }
    else in.ignore();

    Team* newTeam;
    Match* newMatch;
    for (int i = 0; i < numTeams / 2; i++) {    //for number of matches
        newMatch = new Match(in);               //Match constructor
        newTeam = newMatch->returnT1();         //First team
        teams.push_back(newTeam);
        newTeam = newMatch->returnT2();         //Second team
        teams.push_back(newTeam);
        matches.push_back(newMatch);
    }
    if (tournamentStage > 0)
        stageComplete = true;            //Tournament had advanced atelast once

    //Started with 32 teams now 16 left
    if (stageComplete && numTeams / 2 == 16) {
        //Pushes back 2 teams for every iteration, same for the other below
        for (int i = 0; i < numTeams / 4; i++) {
            newMatch = new Match(in);
            sixteenTeams.push_back(newMatch);
        }
        if (tournamentStage > 1)
            stage1Complete = true;
    }
    //Started with 16 teams now 8 left
    if (stageComplete && numTeams / 2 == 8) {
        for (int i = 0; i < numTeams / 4; i++) {
            newMatch = new Match(in);
            quarterFinal.push_back(newMatch);
        }
        if (tournamentStage > 1)
            stage1Complete = true;
    }
    //Started with 8 teams now 4 left
    if (stageComplete && numTeams / 2 == 4) {
        for (int i = 0; i < numTeams / 4; i++) {
            newMatch = new Match(in);
            semiFinal.push_back(newMatch);
        }
        if (tournamentStage > 1)
            stage1Complete = true;
    }
    //Started with 32 teams now 8 left
    if (stage1Complete && numTeams / 2 == 16) {
        for (int i = 0; i < numTeams / 8; i++) {
            newMatch = new Match(in);
            quarterFinal.push_back(newMatch);
        }
        if (tournamentStage > 2)
            stage2Complete = true;
    }
    //Started with 16 teams now 4 left
    if (stage1Complete && numTeams / 2 == 8) {
        for (int i = 0; i < numTeams / 8; i++) {
            newMatch = new Match(in);
            semiFinal.push_back(newMatch);
        }
        if (tournamentStage > 2)
            stage2Complete = true;
    }
    //Started with 32 teams now 4 left
    if (stage2Complete && numTeams / 2 == 16) {
        for (int i = 0; i < numTeams / 16; i++) {
            newMatch = new Match(in);
            semiFinal.push_back(newMatch);
        }
        if (tournamentStage > 3)
            stage3Complete = true;
    }
    //Pushes to final regarding of how many teams the tournament started with
    if (stageComplete && numTeams / 2 == 2 || stage1Complete &&
        numTeams / 2 == 4 || stage2Complete && numTeams / 2 == 8 ||
        stage3Complete && numTeams / 2 == 16) {
        newMatch = new Match(in);
        final.push_back(newMatch);
    }
}

/**
 *  Registers the information needed for the Tournament
 *  The number of teams their names and creates the matches
 * @see Team::readData() and Match::Match(...)
 */
void Tournament::readInfo() {
    bool addPlayers = false;
    bool addPlayers2 = false;
    char tmp;
    cout << "\n\tWhat is the tournament called? ";
    getline(cin, name);
    do {                                //Reads the format of the tournament:
        numTeams = lesInt("\n\tHow many teams will"
            "participate in the tournament(4, 8, 16, 32) ? ", 4, 32);
    } while (numTeams != 4 && numTeams != 8 && numTeams != 16 && numTeams != 32);
    do {
        tmp = lesChar("\n\tDo you want to add players?(Y/N)");
    } while (tmp != 'Y' && tmp != 'N');
    if (tmp == 'Y')
        addPlayers = true;             //Can now addPlayers to the torunament
    if (addPlayers == true) {
        do {                           //Only log goals if addPlayers == true
            tmp = lesChar("Do you want to log the players goals?(Y/N)");
        } while (tmp != 'Y' && tmp != 'N');
        if (tmp == 'Y')
            playerGoals = true;
    }

    Team* newTeam;                                      //creates a new team
    Match* newMatch;                                    //creates a new match
    // for as many teams as the user typed above
    for (int i = 0; i < numTeams; i++) {
        string name;
        int nr;
        cout << "\n\tTeam nr." << i + 1 << ':';
        getline(cin, name);
        cout << endl;
        nr = findTeam(name);                  //If team is already registered
        if (nr != -1) {
            newTeam = gTeams[nr];            //Then the team newTeam becomes it
            teams.push_back(newTeam);
        }
        else {
            newTeam = new Team(name);
            newTeam->readData(addPlayers, addPlayers2);
            teams.push_back(newTeam);       //Pushing to the tournaments vector
            gTeams.push_back(newTeam);      //And global vector of teams.
        }
    }
    //Half as many matches as teams created, first iteration team 1 and 2
    //next iteration 3 and 4... up to the number of teams
    for (int i = 0; i < numTeams; i += 2) {
        //Constructor for matches
        newMatch = new Match(*teams[i], *teams[i + 1]);
        matches.push_back(newMatch);
    }
}

/**
 * Writes the name, date, stage and matches of the tournament
 * @param numMatches - current number of matches in that tournament
 * @see Teams::writeResult()
 */
void Tournament::writeInfo(const int numMatches) const {
    cout << "\n\t" << "Name: " << name << endl;
    cout << '\t' << "Date: ";
    //Date with 0 first on day and months if they are < 10
    if (day < 10) cout << '0';
    cout << day << '/';
    if (month < 10) cout << '0';
    cout << month << '-' << year << endl;

    //Stage of the tournament depending on @param
    if (numMatches == 16)
        cout << "\n\t1st knockout stage:\n";
    else if (numMatches == 8)
        cout << "\n\t2nd knockout stage:\n";
    else if (numMatches == 4)
        cout << "\n\tQuarterfinalists:\n";
    else if (numMatches == 2)
        cout << "\n\tSemifinalists:\n";
    else cout << "\n\tFinalists:\n";

    for (int i = 0; i < numMatches; i++) {
        cout << "\t" << "Match nr." << i + 1 << ": ";

        if (tournamentStage == 0)                         //First stage
            matches[i]->writeResult();
        //second stage and 16 teams left (started with 32 teams)
        else if (tournamentStage == 1 && numMatches == 8)
            sixteenTeams[i]->writeResult();
        //second stage and 8 teams left (started with 16 teams)
        else if (tournamentStage == 1 && numMatches == 4)
            quarterFinal[i]->writeResult();
        //second stage and 4 teams left (started with 8 teams)
        else if (tournamentStage == 1 && numMatches == 2)
            semiFinal[i]->writeResult();
        //third stage and 8 teams left (started with 32 teams)
        else if (tournamentStage == 2 && numMatches == 4)
            quarterFinal[i]->writeResult();
        //third stage and 4 teams left (started with 16 teams)
        else if (tournamentStage == 2 && numMatches == 2)
            semiFinal[i]->writeResult();
        //fourth stage and 4 teams left (started with 32 teams)
        else if (tournamentStage == 3 && numMatches == 2)
            semiFinal[i]->writeResult();
        else final[i]->writeResult();          //FInal stage only 2 teams left
    }
}

/**
* Writes the result for all of the matches in the given stage of the tournament
*
* @see Match::writeResult()
* @param numMatches - number of matches in that stage,
*                     stage - current stage of the tournament (e.g semiFinals)
*/
void Tournament::writeMatches(const int numMatches, vector <Match*> stage) const {
    for (int i = 0; i < numMatches; i++) {             //For number of matches
        cout << "\t" << "Match nr." << i + 1 << ": ";
        stage[i]->writeResult();                //Make match write itself to screen
    }
}
/**
* Writes out all the stages and their matches
*
* @see Tournament::writeMatches (...),
* @param numMatches - number of matches at the beggining of the tournament
*/
void Tournament::viewAll(const int numMatches) const {
    //The name and date of the tournament
    cout << "\n\t" << "Name: " << name << endl;
    cout << '\t' << "Date: ";
    if (day < 10) cout << '0';
    cout << day << '/';
    if (month < 10) cout << '0';
    cout << month << '-' << year << endl;

    //Tournament started with 16 matches (32 teams)
    //"writeMatches" writes the result of all the matches at the wanted stage
    if (numMatches == 16) {
        cout << "\n\t1st knockout stage:\n";
        writeMatches(matches.size(), matches);
        cout << "\n\t2nd knockout stage:\n";
        writeMatches(sixteenTeams.size(), sixteenTeams);
        cout << "\n\tQuarterfinalists:\n";
        writeMatches(quarterFinal.size(), quarterFinal);
        cout << "\n\tSemifinalists:\n";
        writeMatches(semiFinal.size(), semiFinal);
        cout << "\n\tFinalists\n";
        writeMatches(final.size(), final);
    }
    //Started with 8 matches (16 teams)
    else if (numMatches == 8) {
        cout << "\n\t1st knockout stage:\n";
        writeMatches(matches.size(), matches);
        cout << "\n\tQuarterfinalists:\n";
        writeMatches(quarterFinal.size(), quarterFinal);
        cout << "\n\tSemifinalists:\n";
        writeMatches(semiFinal.size(), semiFinal);
        cout << "\n\tFinalists\n";
        writeMatches(final.size(), final);
    }
    //Started with 4 matches (8 teams)
    else if (numMatches == 4) {
        cout << "\n\tQuarterfinalists:\n";
        writeMatches(matches.size(), matches);
        cout << "\n\tSemifinalists:\n";
        writeMatches(semiFinal.size(), semiFinal);
        cout << "\n\tFinalists\n";
        writeMatches(final.size(), final);
    }
    //started with 2 matches (4 teams)

    else if (numMatches == 2) {
        cout << "\n\tSemifinalists:\n";
        writeMatches(matches.size(), matches);
        cout << "\n\tFinalists\n";
        writeMatches(final.size(), final);
    }
}

/*
* Moves the tournament to the next stage for example semifinal->final
* What stage it is pushed to depends on the current stage of the tournament
* And the initial size of the tournament
* Every loop is similair, except number of Iterations and vector it pushes to
*
* @see Match::returnScore(), teamsForNextStage()
*/
void Tournament::nextStage() {
    Match* newMatch = new Match();
    if (matches.size() == 16) {    //Started with 32 teams, now to 16
       teamsForNextStage(matches, matches, 0, newMatch);
    }
                          //Started with 16 teams, now to quarterFinal
    else if (matches.size() == 8 && sixteenTeams.size() == 0
        && tournamentStage == 0) {
        teamsForNextStage(matches, matches, 1, newMatch);
    }
                          //started with 32 teams, now to quarterFinal
    else if (sixteenTeams.size() != 0 && tournamentStage == 1) {
        teamsForNextStage(matches, sixteenTeams, 1, newMatch);
    }
                          //Started in quarterFinal, now to semiFinal
    else if (matches.size() == 4 && quarterFinal.size() == 0
        && tournamentStage == 0) {
        teamsForNextStage(matches, matches, 2, newMatch);
    }
                         //Started with 32 or 8 teams, now to semiFinal
    else if (quarterFinal.size() != 0 && (sixteenTeams.size() != 0
        || tournamentStage == 1)) {
        teamsForNextStage(matches, quarterFinal, 2, newMatch);
    }
                        //Started with 4 teams, now to final
    else if (matches.size() == 2 && semiFinal.size() == 0
        && tournamentStage == 0) {
        teamsForNextStage(matches, matches, 3, newMatch);
    }
    else {              //To final
        teamsForNextStage(matches, semiFinal, 3, newMatch);
    }
}

/*
* Logs the result of the match the user wnats
* If the result for every match have been logged then it goes to the next stage
*
* @see void writeInfo(...), allUpdated(...), result(...), nextStage()
*/
void Tournament::logResult() {
    int choice = 0;
    int tmp = 0;
    //Tournament is now finished
    if (finished == false) {
        do {
            tmp = 0;
            if (tournamentStage == 0) {
                tmp = result(matches);
            }
            else if (tournamentStage == 1 && matches.size() == 16) {
                tmp = result(sixteenTeams);
            }
            else if (tournamentStage == 1 && matches.size() == 8) {
                tmp = result(quarterFinal);
            }
            else if (tournamentStage == 1 && matches.size() == 4) {
                tmp = result(semiFinal);
            }
            else if (tournamentStage == 2 && matches.size() == 16) {
                tmp = result(quarterFinal);
            }
            else if (tournamentStage == 2 && matches.size() == 8) {
                tmp = result(semiFinal);
            }
            else if (tournamentStage == 3 && matches.size() == 16) {
                tmp = result(semiFinal);
            }
            else {
                writeInfo(final.size());
                choice = lesInt("Which game do you want to log result for?"
                                                 "0 = abort", 0, final.size());
                if (choice - 1 != -1) {
                    final[choice - 1]->readResult(playerGoals);
                    if (allUpdated(final)) {      //if The final match finished
                        if (final[0]->returnScore1() > final[0]->returnScore2()) {
                            cout << "\n\tCongratulations "
                                << final[0]->returnT1()->returnName()
                                << "!! You are the champion!";
                            finished = true;          //Tournament is finished
                            winnerName = final[0]->returnT1()->returnName();

                        }
                        else {
                            cout << "\n\tCongratulations "
                                << final[0]->returnT2()->returnName()
                                << "!! You are the champion!";
                            finished = true;          //Tournament is finished
                            winnerName = final[0]->returnT2()->returnName();
                        }
                    }
                }
                else cout << "\n\tNo changes were made!";
            }
        } while (tmp!=0 && finished==false || choice!= 0 && finished == false);
          //User did not type 0
    }
    //If tournament is already finished
    else {
        cout << "\n\tTournament already finished! The winner was team: "
                                                                << winnerName;
    }
}
/**
 * Tournament destructor, deletes all the matches and teams
 */
Tournament :: ~Tournament() {

    //Deletes objects that are pointed to:
    for (int i = 0; i < matches.size(); i++) {
        delete matches[i];
    }
    matches.clear();                            //Deletes all pointers and\
                                                objects being pointed to for all matches

    for (int i = 0; i < sixteenTeams.size(); i++) {
        delete sixteenTeams[i];
    }
    sixteenTeams.clear();

    for (int i = 0; i < quarterFinal.size(); i++) {
        delete quarterFinal[i];
    }
    quarterFinal.clear();

    for (int i = 0; i < semiFinal.size(); i++) {
        delete semiFinal[i];
    }
    semiFinal.clear();

    for (int i = 0; i < final.size(); i++) {
        delete final[i];
    }
    final.clear();

    for (int i = 0; i < teams.size(); i++) {
        delete teams[i];
    }
    teams.clear();
}

/**
* Sends the correct teams to the next stage of the tournament
*
* @param matchess - the vector with matches to determine the winner from,
*       matchesSize - size of the vector matchess, tmp - stage to push to,
*       newMatch - the new match with the winners
* @see Match::returnScore1, Match::returnScore2
*/
void Tournament::teamsForNextStage(vector <Match*> matchess,
                    vector <Match*> matchesSize, int tmp, Match* newMatch) {

    int i = 0;                                  //i(match1,3,5...),
    int j = 1;                                  //j(match2,4,6...)
    int k = 0;                                  //k(team 1 and 2, 5 annd 6...)
    int l = 2;                                  //l(team 3 and 4, 7 and 8...)
    //Two matches for each iteration of the loop
    //4 possible outcomes T1 and T4 wins, T1 and T3 wins,
    //T2 and T3 wins, T2 and T4 wins. Creates the next stages and sends
    //the winning teams to the next stage of the tournament
    for (int i = 0; i < matchesSize.size(); i += 2, j += 2, k += 4, l += 4) {
        if (matchess[i]->returnScore1() > matchess[i]->returnScore2()
            && matchess[j]->returnScore1() > matchess[j]->returnScore2())
            newMatch = new Match(*teams[k], *teams[l]);
        else if (matchess[i]->returnScore1() > matchess[i]->returnScore2()
            && matchess[j]->returnScore1() < matchess[j]->returnScore2())
            newMatch = new Match(*teams[k], *teams[l + 1]);
        else if (matchess[i]->returnScore1() < matchess[i]->returnScore2()
            && matchess[j]->returnScore1() > matchess[j]->returnScore2())
            newMatch = new Match(*teams[k + 1], *teams[l]);
        else newMatch = new Match(*teams[k + 1], *teams[l + 1]);
        //Pushes back the winners to the correct stage depending on @param tmp
        switch (tmp) {
        case 0: sixteenTeams.push_back(newMatch); break;
        case 1: quarterFinal.push_back(newMatch); break;
        case 2: semiFinal.push_back(newMatch);    break;
        case 3: final.push_back(newMatch);        break;
        }
    }
}
/**
* Reads the result and updates win/loss record of the teams.
* Also updates the tournament to the next stage if result for all matches
* are registered
*
* @param matches - the match to log the result of
*/
int Tournament::result(vector <Match*> matches) {
    int choice = 0;
    writeInfo(matches.size());       //Sends the number of matches
    choice = lesInt("Which game do you want to log result for?"
        "0 = abort", 0, matches.size());
    if (choice - 1 != -1) {
        matches[choice - 1]->readResult(playerGoals);  //Updates result
        if (allUpdated(matches)) {        //If all matches==updated
            nextStage();                  //Move to next stage
            ++tournamentStage;            //Update tournament stage
        }
    }
    else cout << "\nNo changes were made!";
    return choice;
}

/**
*   Writes tournament to file
*
*   @param out - the output file
*/
void Tournament::writeToFile(ofstream& out) {
    bool stageComplete = false;
    bool stage1Complete = false;
    bool stage2Complete = false;
    bool stage3Complete = false;
    //Writes the simple private data to file
    out << day << ' ' << month << ' ' << year << '\n';
    out << name << '\n';
    out << numTeams << '\n';
    out << tournamentStage << '\n';
    out << finished << '\n' << playerGoals << '\n';
    if (finished)
        out << winnerName << '\n';
    else out << '\n';

    //Writes the matches of the first stage to file
    for (int i = 0; i < numTeams / 2; i++) {
        matches[i]->writeToFile(out);
    }
    if (tournamentStage > 0)                //Tournament has advanced once
        stageComplete = true;

    if (stageComplete && numTeams / 2 == 16) {//Second stage to is sixteenTeams
        for (int i = 0; i < numTeams / 4; i++) {
            sixteenTeams[i]->writeToFile(out);
        }
        if (tournamentStage > 1)
            stage1Complete = true;         //Tournament has advanced twice
    }
    if (stageComplete && numTeams / 2 == 8) {    //Second stage is quarterFinal
        for (int i = 0; i < numTeams / 4; i++) {
            quarterFinal[i]->writeToFile(out);
        }
        if (tournamentStage > 1)
            stage1Complete = true;       //Tournament has advanced twice
    }
    if (stageComplete && numTeams / 2 == 4) {   //second stage is semifinal
        for (int i = 0; i < numTeams / 4; i++) {
            semiFinal[i]->writeToFile(out);
        }
        if (tournamentStage > 1)
            stage1Complete = true;       //Tournament has advanced twice
    }
    if (stage1Complete && numTeams / 2 == 16) { //Third stage is quarterFinal
        for (int i = 0; i < numTeams / 8; i++) {
            quarterFinal[i]->writeToFile(out);
        }
        if (tournamentStage > 2)
            stage2Complete = true;      //Tournament has advanced three times
    }
    if (stage1Complete && numTeams / 2 == 8) {  //Third stage is semiFinals
        for (int i = 0; i < numTeams / 8; i++) {
            semiFinal[i]->writeToFile(out);
        }
        if (tournamentStage > 2)
            stage2Complete = true;     //Tournament has advanced three times
    }
    if (stage2Complete && numTeams / 2 == 16) { //Fourth stage is semiFinals
        for (int i = 0; i < numTeams / 16; i++) {
            semiFinal[i]->writeToFile(out);
        }
        if (tournamentStage > 3)
            stage3Complete = true;   //Tournament has advanced four times
    }
    //Finals if the tournament has advanced atleast once
    if (stageComplete && numTeams / 2 == 2 || stage1Complete &&
        numTeams / 2 == 4 || stage2Complete && numTeams / 2 == 8 ||
        stage3Complete && numTeams / 2 == 16) {
        final[0]->writeToFile(out);
    }
}
