#ifndef __MATCH_H
#define __MATCH_H
#include "team.h"
#include <fstream>
/**
 *  Base class 'Match'.
 *  Contains info on the two competing teams and their score.
 */
class Match {
private:
    Team* team1;
    Team* team2;
    int scoreT1 = 0;
    int scoreT2 = 0;
    bool updated = false;

public:
    Match(std::ifstream& in);
    Match(Team& t1, Team& t2);
    Match() { team1 = team2 = nullptr; scoreT1 = scoreT2 = 0; updated = false; };
    int returnScore1() { return scoreT1; }
    int returnScore2() { return scoreT2; }
    void readResult(const bool playerGoals);
    void writeResult() const;
    Team* returnT1() { return team1; }
    Team* returnT2() { return team2; }
    bool returnUpdated() { return updated; }
    void writeToFile(std::ofstream& out);
};
#endif // __MATCH_H
