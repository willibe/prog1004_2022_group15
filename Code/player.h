#ifndef __PLAYER_H
#define __PLAYER_H
#include <string>
#include <fstream>
/**
 *  Base class 'Player'.
 *  Contains first name, last name and number of goals scored.
*/
class Player {
private:
    std::string firstName;
    std::string lastName;
    int numGoals, team;

public:
    Player() { firstName = "", lastName = "", numGoals = 0; }
    Player(std::ifstream& in);
    ~Player(){};
    virtual void readData();
    virtual void writeData();
    bool hasName(std::string n, std::string n2);
    void updateGoals(Player* p1, const int goals)
    {
        p1->numGoals += goals;
    }
    void setData(std::string fName, std::string lName, int teamID);
    std::string returnFirstName() { return firstName; }
    std::string returnLastName() { return lastName; }
    int returnNumGoals() { return numGoals; }
    int returnTeam() { return team; }
    void writeToFile(std::ofstream& out);
};
#endif // __PLAYER_H

