#include "tournament.h"
#include "match.h"
#include "LesData2.h"
#include "team.h"
#include "player.h"
#include "functions.h"
#include <vector>
#include <string>
#include <iostream>
#include <algorithm>
#include <iomanip>

using namespace std;
extern vector <Tournament*> gTournaments;
extern vector <Team*> gTeams;
extern vector <Player*> gPlayers;

/**
 * Reads the Tournaments data from file
 *
 * @see Player::Player(...), Team::Team(...), Tournament::Tournament(...)
 */
void readFromFile() {
        //READ PLAYERS:
    ifstream playerFile("PLAYERS.DTA");
    Player* newPlayer;
    if (playerFile)     // If the file exists
    {
        int numPlayers;
        playerFile >> numPlayers;       //Reads how many players are on the file
        playerFile.ignore();
        for (int i = 0; i < numPlayers; i++) {
            newPlayer = new Player(playerFile); //Player reads itself from file
            gPlayers.push_back(newPlayer);      //Save player to global vector
        }
        cout << "READ PLAYERS FROM 'PLAYERS.DTA'\n";
        playerFile.close();         //Close file
    }
    else
        cout << "Could not open the file 'PLAYERS.DTA'!\n";

        //READ TEAMS:
    ifstream teamFile("TEAMS.DTA");
    Team* newTeam;
    if (teamFile)     // If the file exists
    {
        int numTeams;
        teamFile >> numTeams;       //Reads how many teams are on the file
        teamFile.ignore();
        for (int i = 0; i < numTeams; i++) {
            newTeam = new Team(teamFile, true); //Team reads itself from file
            gTeams.push_back(newTeam);          //Saves team to global vector
        }
        cout << "READ TEAMS FROM 'TEAMS.DTA'\n";
        teamFile.close();
    }
    else
        cout << "Could not open the file 'TEAMS.DTA'!\n";

        //READ TOURNAMENTS:
    ifstream tournamentFile("TOURNAMENTS.DTA");
    Tournament* newTournament;
    if (tournamentFile)     // If the file exists
    {
        int numTournaments = 0;
        tournamentFile >> numTournaments;
        tournamentFile.ignore();
        for (int i = 0; i < numTournaments; i++) {           //All tournaments
            newTournament = new Tournament(tournamentFile); //Tournament reads itself
            gTournaments.push_back(newTournament);      //Saves tournament to vector
        }
        cout << "READ TOURNAMENTS FROM 'TOURNAMENTS.DTA'\n";
        tournamentFile.close();
    }
    else
        cout << "Could not open the file 'TOURNAMENS.DTA'!\n";
}

/**
 *  Writes players, teams and tournaments to file
 *
 * @see Player::writeToFile(...)
 * @see Team::writeToFile(...)
 * @see Tournament::writeToFile(...)
 */
void writeToFile() {
        //WRITE PLAYERS:
    ofstream playerFile("PLAYERS.DTA");
    if (playerFile)     // If the file exists
    {
        playerFile << gPlayers.size() << '\n';      //Writes how many players are saved
        for (int i = 0; i < gPlayers.size(); i++) {
            gPlayers[i]->writeToFile(playerFile);   //Every player writes itself to file
        }
        cout << "WROTE PLAYERS TO 'PLAYERS.DTA'\n";
        playerFile.close();         //Closes file
    }
    else
        cout << "Could not open the file 'PLAYERS.DTA'!\n";

        //WRITE TEAMS:
    ofstream teamFile("TEAMS.DTA");
    if (teamFile)     // If the file exists
    {
        teamFile << gTeams.size() << '\n';      //Writes how many teams are saved
        for (int i = 0; i < gTeams.size(); i++) {
            gTeams[i]->writeToFile(teamFile, true); //Every team writes itself to file
        }
        cout << "WROTE TEAMS TO 'TEAMS.DTA'\n";
        teamFile.close();
    }
    else
        cout << "Could not open the file 'TEAMS.DTA'!\n";

        //WRITE TOURNAMENTS:
    ofstream tournamentFile("TOURNAMENTS.DTA");
    if (tournamentFile)     // If the file exists
    {
        tournamentFile << gTournaments.size() << '\n';  //Write number of tournaments
        for (int i = 0; i < gTournaments.size(); i++) {
            gTournaments[i]->writeToFile(tournamentFile);   //Write itself to file
        }
        cout << "WROTE TOURNAMENTS TO 'TOURNAMENTS.DTA'\n";
        tournamentFile.close();
    }
    else
        cout << "Could not open the file 'TOURNAMENS.DTA'!\n";
}

/**
 *  Writes all viewable options to the screen
 *
 * @see viewTournament(), viewTeams(), viewPlayers().
 */
void view()
{
    //Prints all viewable options
    cout << "\t1 - Tournaments" << endl;
    cout << "\t2 - Teams" << endl;
    cout << "\t3 - Players" << endl;
    cout << "\t0 - Return" << endl;

    int valg =
        lesInt("What do you want to view?", 0, 3);
    //Runs function according to user input above
    if (valg == 1)
        viewTournaments();
    else if (valg == 2)
        viewTeams();
    else if (valg == 3)
        viewPlayers();
    cout << "\nGoing back to menu . . ." << endl;
}

/**
 *  Writes all tournaments to te console
 *
 * @see Tournament::returnName (), Tournament::viewAll(...),
 * @see Tournament::returnMatches()
 */
void viewTournaments() {
    if (gTournaments.size() > 0) {
        //Shows the name of all tournaments in the program:
        for (int i = 0; i < gTournaments.size(); i++) {
            cout << "\n\tTournament nr." << i + 1 << ' '
                << gTournaments[i]->returnName() << endl;
        }
        //Lets the user choose which tournament they want to view:
        int valg =
            lesInt("Which tournament do you want to view?", 0, gTournaments.size());
        valg--;

        if (valg != -1) {
            gTournaments[valg]->viewAll(gTournaments[valg]->returnMatches().size());
        }
    }
    else
        cout << "\n\tNo tournaments saved!\n";
}

/**
 *  Writes all teams to the console
 *
 * @see Teams::writeData(...)
 */
void viewTeams() {
    char svar;
    if(gTeams.size() > 0){      //There are one or more teams registered
        do {
            svar =
                lesChar("Do you want to list teams AND players? (Y = Yes, N = No)");
        } while ((svar != 'Y') && (svar != 'N'));

        //For all teams registered, write their data with out without players
        for (int i = 0; i < gTeams.size(); i++) {
            cout << "\n\tTeam nr. " << i + 1 << ": ";
            if (svar == 'Y') {
                gTeams[i]->writeData(true);
            }
            else if (svar == 'N')
                gTeams[i]->writeData(false);
        }
    }
}

/**
 *  Writes all players to screen
 *
 * @see Player::returnNumGoals(), Player::returnTeam(),
 *      Player::returnFirstName(), Player::returnLastName().
 */
void viewPlayers() {
    string name;
    if (gPlayers.size() > 0) {    //At least one or more players are registered
        cout << "\n\nPlayers:\n";
        //Write all the players data
        for (int i = 0; i < gPlayers.size(); i++) {
            name = gPlayers[i]->returnFirstName() + " "
                + gPlayers[i]->returnLastName();
            cout << '\t'
                << left << setw(40) << name << "\tGoals: "
                << gPlayers[i]->returnNumGoals() << "\t\t ("
                << gTeams[gPlayers[i]->returnTeam()]->returnName()
                << ")" << endl;
        }
    }
    else
        cout << "\n\tNo players are saved!\n";
}

/**
 *  Updates the results of a tournament
 *
 * @see  Tournament::logResult(), Tournament::returnName()
 */
void updateTournament() {
    int choice2 = 0;
        //Displays name all tournaments registered with number
        for (int i = 0; i < gTournaments.size(); i++) {
            cout << "\n\tTournament nr." << i + 1 << ' '
                << gTournaments[i]->returnName() << endl;
        }
        int choice = lesInt("\nWhich tournament do you want to change?",
            0, gTournaments.size());
        if (choice - 1 == -1)
            cout << "\nNo changes were made!\n";
        else                    //Update result of the desired tournament
            gTournaments[choice - 1]->logResult();

    cout << "\nGoing back to menu . . ." << endl;
}

/**
 *  Frees allocated memory
 */
void freeAllocatedMemory() {
        //Deletes all tournaments:
    for (int i = 0; i < gTournaments.size(); i++) {
        delete gTournaments[i];
    }
    gTournaments.clear();
        //Deletes all players:

    for (int i = 0; i < gPlayers.size(); i++) {
        delete gPlayers[i];
    }
    gPlayers.clear();
        //Deletes all teams:
    for (int i = 0; i < gTeams.size(); i++) {
        //delete gTeams[i];
    }
    gTeams.clear();
}

/**
 * Checks for an unambiguous name match in saved teams
 *
 * @param n - name to be checked against
 * @see Team::returnName(), Team::hasName()
 * @return - index-number of found team, -1 if not found or name is ambiguous
 */
int findTeam(string n) {
    string name;
    int index = -1;
    //Transforms n to capital letters:
    std::transform(n.begin(), n.end(), n.begin(), ::toupper);
    //Checks for identical name:
    for (int i = 0; i < gTeams.size(); i++) {
        name = gTeams[i]->returnName();
        std::transform(name.begin(), name.end(), name.begin(), ::toupper);
        if (name == n)
            index = i;
    }
    //if no identical match was found, search for unambiguous match:
    if (index == -1) {
        for (int i = 0; i < gTeams.size(); i++) {
            if (gTeams[i]->hasName(n)) {
                if (index == -1)   //If no unambiguous match has been found yet
                    index = i;
                else return -1;   //If one already matched, the name is ambiguous
            }
        }
    }
    return index;
}


/**
 *  Creates a new tournament
 *
 * @see Tournament::readInfo(), checkDate(...)
 */
void newTournament() {
    int day, month, year;
    Tournament* tournament;     // Pointer to a tournament class

    cout << "\tCreate tournament nr." << gTournaments.size() + 1 << endl;
    do {
        cout << "\tWhen is the tournament (With spaces, example: 11 12 2022)?: ";
        cin >> day >> month >> year; cin.ignore();
    } while (!checkDate(day, month, year));

    tournament = new Tournament(day, month, year);

    tournament->readInfo();     // The rest of the tournament info
                                          //is filled in through this FUNCTIONS
    gTournaments.push_back(tournament);
    // Once all the info is filled in, the pointer to the tournament is
                                           // added to the vector gTournaments.
}

/**
 *  Creates a new team
 *
 * @see findTeam(...), Team::readData(...)
 */
void newTeam() {
    string name;
    Team* nTeam;
            //Only accepts a new name:
    do {
        cout << "\nWrite the name of the new team (abort with empty name): ";
        getline(cin, name);
    } while (findTeam(name) != -1 && name != "");
        //If user didn't try to abort:
    if (name != "") {
        nTeam = new Team(name);
            //Makes user write info about team:
        nTeam->readData(true, true);
        gTeams.push_back(nTeam);
    }
    else
        cout << "\nAborted creation of new team!\n";
}

/**
* Deletes a specific tournament of the users choice, or all tournaments.
*
* @see Tournaments::returnName(), deleteAllTournaments(), deleteASpecific(...)
*/
void deleteTournament() {
    if (gTournaments.size() != 0) {
        for (int i = 0; i < gTournaments.size(); i++) {
            cout << "\n\tTournament nr." << i + 1
                << ' ' << gTournaments[i]->returnName();
        }

        cout << "\n\n    -1:  Deletes all Tournaments."
            << "\n     0:  Does not delete any Tournament."
            << "\n (1-" << gTournaments.size() << "):  "
            << "Deletes a spesific Tournament.\n";

        int valg = lesInt("\n\tWhich Tournament do you want to delete?",
            -1, gTournaments.size());
        //Deletes all Tournaments.
        if (valg == -1) {
            cout << "\nDeletes All Tournaments . . .\n";
            deleteAllTournaments();
        }
        //Does not delete any Tournament, "Regret" button.
        else if (valg == 0) {
            cout << "\nNo Tournament is deleted." << endl;
        }
        //Deletes a specific Tournament.
        else
            deleteASpecific(valg);
    }
    else
        cout << "\nThere are no Tournaments, nothing to delete." << endl;
}
/**
* Deletes all tournaments registered.
*/
void deleteAllTournaments() {
    for (int i = 0; i < gTournaments.size(); i++)    //Deletes all tournaments
        delete gTournaments[i];                      //         in the struct.
    gTournaments.clear();

}
/**
* Deltes a given tournament
*
* @param nr - number of the tournament in the vector gTournaments
*/
void deleteASpecific(const int nr) {
    delete gTournaments[nr - 1];
    //Moves the tournament to be deleted to the end of the vector
    gTournaments[nr - 1] = gTournaments[gTournaments.size() - 1];
    gTournaments.pop_back();
    cout << "\n\tThe spesific Tournament is now Deleted" << endl;
}


/**
 *  Writes the menu for the program to the screen
 *  @param d - day to check
 *  @param m - month to check
 *  @param y - year to check
 *  @return - true if date is valid, else false
 */
bool checkDate(const int d, const int m, const int y) {
    return (d >= 1 && d <= 31 && m >= 1 && m <= 12 && y >= 2022 && y <= 2035);
}
/*
* Checks if the result of every match in the current tournament is registered
*
* @param stage - current stage of the tournament
* @return - If all matches are updated(true) or not(false)
*/
bool allUpdated(vector <Match*> stage) {
    for (int i = 0; i < stage.size(); i++) {    //for every match in that stage
        if (!stage[i]->returnUpdated())                 //Not updated
            return false;
    }
    return true;                               //Every match was updated
}


/**
 *  Writes the menu for the program to the screen
 */
void writeMenu() {
    cout << "\nChoose a command:\n"
        << "\tN - New tournament\n"
        << "\tT - New team\n"
        << "\tV - View...\n"
        << "\tU - Update...\n"
        << "\tD - Delete one or all tournaments\n"
        << "\tQ - Quit\n";
}
