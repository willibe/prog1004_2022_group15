#ifndef __TEAM_H
#define __TEAM_H
#include <vector>
#include <fstream>
#include <string>


/**
 * Base class 'Team'
 * contains the teamName, players of the team and the teams number of wins,
 * losses and uunique id.
 */
class Team {
private:
    std::string teamName;
    std::vector <int> players;              // Vector of pointers
                                            //to the players index in gPlayers
    int numWins, numLosses, id;

public:
    Team() { teamName = ""; numWins = numLosses = id = 0; };
    Team(std::ifstream& in, const bool allData);
    ~Team(){};
    Team(const Team& team);
    Team(std::string n) { teamName = n; numWins = numLosses = id = 0; }
    std::string returnName() { return teamName; }
    virtual void readData(const bool addPlayers, const bool addPlayers2);
    virtual void writeData(bool all) const;
    bool hasName(std::string n);
    std::vector <int> returnPlayers() { return players; }
    void updateWins() { numWins += 1; }
    void updateLosses() { numLosses += 1; }
    void writeToFile(std::ofstream& out, const bool allData);
};
#endif // __TEAM_H
