#include "LesData2.h"
#include "match.h"
#include "team.h"
#include "player.h"
#include "functions.h"
#include <fstream>
#include <string>
#include <iostream>
using namespace std;
extern vector <Player*> gPlayers;
extern Player* playerFunctions;
extern vector <Team*> gTeams;

/**
 *  Constructor for reading tournament from file
 *
 * @param in - the file to be read from
 */
Match::Match(ifstream& in) {
    int help = 0;
    team1 = new Team(in, false);             //False - only read the teams name
    team2 = new Team(in, false);
    in >> scoreT1 >> scoreT2 >> help;        //Saving scores
    if (help == 1)                           //Match has been updated
        updated = true;
    else
        updated = false;
    in.ignore();
}

/**
* Constructor for class matches
* creates two new teams and sets their score to 0
*
* @param t1 - first team, t2 - second team.
*/
Match::Match(Team& t1, Team& t2) {
    team1 = new Team(t1);
    team2 = new Team(t2);
    scoreT1 = 0;
    scoreT2 = 0;
}

/**
 *  Reads in the score of both teams. And updtaes vriabls accordingly
 * 
 * @param playerGoals - if goals per player should be read
 * @see Team::returnName(), Team::updateWins(), Team::updateLosses()
 * @see Team::returnPlayers(), Player::updateGoals(), Player::returnFirstName()
 * @see Player::returnLastName()
 */
void Match::readResult(const bool playerGoals) {
    string name;
    vector <int> players;
    int playerNumber = 0;
    int goals = 0;
    int playerNumGoals = 0;
    int updatedScore = 0;
    do {
        //Result for the match
        cout << "How many goals did " << team1->returnName() << " score?";
        scoreT1 = lesInt("", 0, 99);
        cout << "How many goals did " << team2->returnName() << " score?";
        scoreT2 = lesInt("", 0, 99);
        if (scoreT1 == scoreT2)   //Knockout stage cant end with draw
            cout << "\n\tThe match can not end with a draw!\n";
        if (scoreT1 > scoreT2) {  //Updating wins and losses for each team
            gTeams[findTeam(team1->returnName())]->updateWins();
            gTeams[findTeam(team2->returnName())]->updateLosses();
        }
        if (scoreT2 > scoreT1) {
            gTeams[findTeam(team1->returnName())]->updateLosses();
            gTeams[findTeam(team2->returnName())]->updateWins();
        }
    } while (scoreT1 == scoreT2);

    if (playerGoals == true) {
        players = team1->returnPlayers();
        cout << "\n\tTeam 1 Players:";
        updatedScore = scoreT1;
        for (int i = 0; i < players.size(); i++) { //for the teams players
            //Write their names and attached numbers 
            name = gPlayers[players[i]]->returnFirstName() + " "
                + gPlayers[players[i]]->returnLastName();
            cout << "\n\t\tPlayer number " << i + 1 << ": " << name;
        }
        do {
            playerNumber = lesInt("\n\t\tWho scored the goals for Team 1 "
                "(player number)?:", 1, players.size());
            playerNumGoals = lesInt("\n\t\tHow many goals did "
                "this player score?", 0, updatedScore);
            updatedScore -= playerNumGoals; //Decreasing goals left to register
            playerFunctions->updateGoals(gPlayers[players[playerNumber - 1]],
                         playerNumGoals); //Player that scored the goal/goals
        //Run until there are no more goals to register
        } while (updatedScore > 0);

        players = team2->returnPlayers();
        cout << "\n\tTeam 2 Players:";
        updatedScore = scoreT2;
        for (int i = 0; i < players.size(); i++) {
            name = gPlayers[players[i]]->returnFirstName() + " "
                + gPlayers[players[i]]->returnLastName();
            cout << "\n\t\tPlayer number " << i + 1 << ": " << name;
        }
        do {
            playerNumber = lesInt("\n\t\tWho scored the goals for Team 2 "
                "(player number)?:", 1, players.size());
            playerNumGoals = lesInt("\n\t\tHow many goals did "
                "this player score?", 0, updatedScore);
            updatedScore -= playerNumGoals;
            playerFunctions->updateGoals(gPlayers[players[playerNumber - 1]],
                playerNumGoals);
        } while (updatedScore > 0);
    }
    updated = true;                                   //Match is now finished
}

/**
 *  Writes out both teams names and their score against each other.
 * 
 * @see Team::returnName()
 */
void Match::writeResult() const {
    cout << team1->returnName() << ' ' << scoreT1
        << '-' << scoreT2 << ' ' << team2->returnName() << '\n';
}

/**
*   Writes tournament to file
*
*   @param out - the output file
*   @see Team::writeToFile(...)
*/
void Match::writeToFile(ofstream& out) {
    team1->writeToFile(out, false);             //Only write teams name
    team2->writeToFile(out, false);
    out << scoreT1 << ' ' << scoreT2 << ' ' << updated << '\n';
}
